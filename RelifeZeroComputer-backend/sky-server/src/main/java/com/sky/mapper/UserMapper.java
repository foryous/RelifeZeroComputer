package com.sky.mapper;

//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//


import com.github.pagehelper.Page;
import com.sky.annotation.AutoFill;
import com.sky.enumeration.OperationType;
import com.sky.pojo.Remind;
import com.sky.pojo.User;
import com.sky.pojo.UserDTO;
import com.sky.pojo.UserPageQueryDTO;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface UserMapper {
    @Select({"select * from log.user where username =#{username}"})
    User getByUsername(String username);

    @AutoFill(OperationType.UPDATE)
    void updateUser(User user);

    @Select({"select * from log.user where id=#{id}"})
    User selectbyid(Long id);

    @Select({"select * from log.user where email=#{email}"})
    User selectbyemail(String email);

    @AutoFill(OperationType.INSERT)
    void insertuser(User user);

    @Select({"select * from log.user where username=#{username}"})
    User selectbyusername(String username);

    Page<User> findpage(UserPageQueryDTO userPageQueryDTO);

    @Delete({"delete from log.user where id=#{id}"})
    void deletebyid(Long id);

    @Select("select * from log.user where study_status=#{studyStatus} and remind_status =#{remindStatus}")
    List<User> selectbystudystatusandremind(Remind remind);

    @AutoFill(OperationType.UPDATE)
    void updateUserstudystatus(Integer studyStatus);


    Page<User> findpageorderbyscore(UserPageQueryDTO userPageQueryDTO);

    @Select("select * from log.user where user_login=#{userLogin} ")
    List<User> selectbyuserlogin(Integer userLogin);

    void updateUserbyusername(User user);
    @Select("select * from log.user where study_status=#{studyStatus}")
    List<User> selectbystudystatus(Integer studyStatus);
}
