package com.sky.controller;
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
import com.sky.pojo.Log;
import com.sky.pojo.LogPageQueryDTO;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.LogService;
import io.swagger.annotations.Api;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
@Slf4j
@RestController
@RequestMapping({"/user/log"})
@CrossOrigin
@Api(
        tags = {"log备忘录相关接口"}
)
public class LogController {
    @Autowired
    private LogService logService;
    @PostMapping
    public Result addcontent(@RequestBody Log log) {
        this.logService.addcontent(log);
        return Result.success();
    }
    @DeleteMapping
    public Result delectbyid(@RequestParam List<Long> ids) {
        this.logService.delectbylogid(ids);
        return Result.success();
    }

    @GetMapping({"/page"})
    public Result<PageResult> page(LogPageQueryDTO logPageQueryDTO) {
        PageResult page = this.logService.page(logPageQueryDTO);
        return Result.success(page);
    }

    @GetMapping({"/{id}"})
    public Result<Log> selcetbyid(@PathVariable Long id) {
        Log log = this.logService.selectbyid(id);
        return Result.success(log);
    }

    @PutMapping
    public Result updatebyid(@RequestBody Log log) {
        this.logService.updatebyid(log);
        return Result.success();
    }
}
