package com.sky.interceptor;

import com.fasterxml.jackson.databind.ser.Serializers;
import com.sky.constant.JwtClaimsConstant;
import com.sky.constant.MessageConstant;
import com.sky.context.BaseContext;
import com.sky.exception.UserFailLoginException;
import com.sky.exception.UserStatusLockedException;
import com.sky.mapper.UserLoginMapper;
import com.sky.mapper.UserMapper;
import com.sky.pojo.User;
import com.sky.pojo.UserLogin;
import com.sky.properties.JwtProperties;
import com.sky.utils.JwtUtil;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;

/**
 * jwt令牌校验的拦截器
 */
@Component
@Slf4j
public class JwtTokenUserInterceptor implements HandlerInterceptor {

    @Autowired
    private JwtProperties jwtProperties;
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private UserLoginMapper userLoginMapper;
    @Autowired
    private UserMapper userMapper;

    /**
     * 校验jwt
     *
     * @param request
     * @param response
     * @param handler
     * @return
     * @throws Exception
     */
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (!(handler instanceof HandlerMethod)) {
            return true;
        } else {
            String token = request.getHeader(this.jwtProperties.getAdminTokenName());

            try {
                log.info("jwt校验:{}", token);
                Claims claims = JwtUtil.parseJWT(this.jwtProperties.getAdminSecretKey(), token);
                Long userid = Long.valueOf(claims.get(JwtClaimsConstant.USER_ID).toString());
                User selectbyuserid = userMapper.selectbyid(userid);
                Integer userLogin1 = selectbyuserid.getUserLogin();
                if (userLogin1 == 0) {
                    throw new UserFailLoginException(MessageConstant.USER_NOT_LOGIN);
                }
                ValueOperations valueOperations = redisTemplate.opsForValue();
                String userstatus = (String) valueOperations.get(JwtClaimsConstant.USER_STATUS + userid);
                String userpower = (String) valueOperations.get(JwtClaimsConstant.USER_POWER + userid);
                Integer userStatus = Integer.valueOf(userstatus);
                Integer userPower = Integer.valueOf(userpower);
                BaseContext.setUserId(userid);
                BaseContext.setStatus(userStatus);
                BaseContext.setPower(userPower);
                log.info("当前用户id:{}", userid);
                if(userPower != 1)
                {log.info("当前用户权限:{}", (userPower == 0 ? "普通用户" : "vip用户"));}
                else {
                    log.info("当前用户权限:{}", "管理员用户");
                }
                log.info("当前用户状态:{}", userStatus == 0 ? "禁止状态" : "正常状态");
                LocalDateTime now = LocalDateTime.now();
                User user = new User();
                user.setId(userid);
                user.setLastime(now);
                userMapper.updateUser(user);
                if (userStatus == 0) {
                    throw new UserStatusLockedException(MessageConstant.USER_LOCKED);
                }
                return true;
            } catch (UserStatusLockedException e) {
                response.setStatus(789);
                return false;
            } catch (UserFailLoginException e) {
                response.setStatus(678);
                return false;
            } catch (Exception var8) {
                response.setStatus(401);
                return false;
            }


        }
    }
}
