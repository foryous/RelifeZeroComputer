package com.sky.mapper;

//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//


import com.sky.annotation.AutoFill;
import com.sky.enumeration.OperationType;
import com.sky.pojo.LogUser;
import java.util.List;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface LogUserMapper {
    @Select({"select log_id from log.log_user where user_id=#{id}"})
    List<Long> selectbyuserid(Long id);


    void addcontact(LogUser logUser);

    @Delete({"delete from log.log_user where log_id=#{id}"})
    void delectbylogid(Long id);

    @Delete({"delete from log.log_user where user_id=#{id}"})
    void deletebyuserid(Long id);
    @Select("select user_id from log.log_user where log_id =#{logId} ")
    Long selectbylogid(Long logId);
}

