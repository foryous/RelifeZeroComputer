package com.sky.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MessageVO implements Serializable {
    private Long id;

    private String username;

    private String title;

    private String content;

    private LocalDateTime createTime;

    private String bz;

    private Long createUser;
}
