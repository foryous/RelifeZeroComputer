package com.sky.service;

import com.sky.pojo.Sky;

import java.util.List;

public interface SkyService {
    void updatebyid(Sky sky);

    void deletebyids(List<Long> ids);

    void addsky(Sky sky);
}
