// 引用axios
import axios from "axios";
import {Message} from 'element-ui';
import {Getfromcookie } from "../javaScript/Cookie";

// 创建一个新的axios接口配置
// instance是一个向后端的接口配置
const instance=axios.create({
    baseURL:"http://www.foryousylnx.cn:9090"

});

instance.interceptors.request.use(function(config){

    //通过cookie的方式获取用户token数据
    let Usertoken=Getfromcookie().token;
    if(Usertoken){
        //token获取成功在请求头上加上用户的jwt令牌
        config.headers["token"]=Usertoken;
    }
    else {
        //获取失败在请求头上加上错误的jwt令牌
        config.headers["token"]=null;
    }
    return config;
},function(error){
    //$router.push("/login");
    Message("错误")
    return Promise.reject(error);
});


//响应拦截器
instance.interceptors.response.use(function(response){
    if(response.data.code===200){
        return response.data;
    }
    if(response.data.code===201){
        Message("密钥效验错误，请联系管理员")
    }
    return response.data;
})

export function get(url,params){
    return instance.get(url,{params})
};

export function post(url,data){
    return instance.post(url,data)
};

export function put(url,data){
    return instance.put(url,data)
};

export function dele(url,data){
    return instance.delete(url,data)
};
