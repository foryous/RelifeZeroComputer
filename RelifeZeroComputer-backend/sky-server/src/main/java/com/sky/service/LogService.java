package com.sky.service;
import com.sky.pojo.Log;
import com.sky.pojo.LogPageQueryDTO;
import com.sky.result.PageResult;
import java.util.List;

public interface LogService {
    void addcontent(Log log);

    void delectbylogid(List<Long> id);

    PageResult page(LogPageQueryDTO logPageQueryDTO);

    Log selectbyid(Long id);

    void updatebyid(Log log);
}
