package com.sky.service.impl;

//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//


import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.MessageConstant;
import com.sky.context.BaseContext;
import com.sky.exception.UserPowerLowException;
import com.sky.mapper.LogMapper;
import com.sky.mapper.LogUserMapper;
import com.sky.mapper.UserMapper;
import com.sky.pojo.*;
import com.sky.result.PageResult;
import com.sky.service.LogService;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class LogServicecImpl implements LogService {

    @Autowired
    private LogMapper logMapper;
    @Autowired
    private LogUserMapper logUserMapper;
    @Autowired
    private UserMapper userMapper;

    @Transactional
    public void addcontent(Log log) {
        Long userId = BaseContext.getUserId();
        User selectbyid = userMapper.selectbyid(userId);
        Integer showStatus = selectbyid.getShowStatus();
        String email = selectbyid.getEmail();
        Pattern pattern = Pattern.compile("\\d+"); // 创建匹配数字的正则表达式模式
        Matcher matcher = pattern.matcher(email); // 在字符串 s 中查找匹配模式的字符串
        matcher.find();
        String qq = matcher.group();
        log.setQQnumber(qq);
        log.setShowStatus(showStatus);
        this.logMapper.addcontent(log);
        Long logId = log.getId();
        LogUser logUser = new LogUser();
        logUser.setUserId(userId);
        logUser.setLogId(logId);
        this.logUserMapper.addcontact(logUser);
        Integer studyStatus = selectbyid.getStudyStatus();
        if (studyStatus == 0) {
            Integer duringtime = selectbyid.getDuringtime();
            Long score = selectbyid.getScore();
            duringtime += 1;
            if (duringtime > 0 && duringtime <= 5) {
                score += 1;
            } else if (duringtime > 5 && duringtime <= 11) {
                score += 3;
            } else if (duringtime > 11 && duringtime <= 20) {
                score += 5;
            } else if (duringtime > 20) {
                score += 6;
            }
            User user = new User();
            user.setStudyStatus(1);
            user.setId(userId);
            user.setDuringtime(duringtime);
            user.setScore(score);
            userMapper.updateUser(user);
        }

    }

    @Transactional
    public void delectbylogid(List<Long> ids) {
        Iterator var2 = ids.iterator();
        Long userId = BaseContext.getUserId();
        Integer power = BaseContext.getPower();
        while (var2.hasNext()) {
            Long id = (Long) var2.next();
            Long selectbylogid = logUserMapper.selectbylogid(id);
            if(power != 1) {
                if (!userId.equals(selectbylogid)) {
                    throw new UserPowerLowException(MessageConstant.NOPOWER_TOUPDATE);
                }
            }
            this.logMapper.delectbyid(id);
            this.logUserMapper.delectbylogid(id);
        }

    }

    public PageResult page(LogPageQueryDTO logPageQueryDTO) {
        PageHelper.startPage(logPageQueryDTO.getPage(), logPageQueryDTO.getPageSize());
        Page<Log> page = this.logMapper.findpage(logPageQueryDTO);
        long total = page.getTotal();
        List<Log> result = page.getResult();
        return new PageResult(total, result);
    }

    public Log selectbyid(Long id) {
        Log log = this.logMapper.selectbyid(id);
        return log;
    }

    public void updatebyid(Log log) {
        Long logId = log.getId();
        Long userId=BaseContext.getUserId();
        Long uid=logUserMapper.selectbylogid(logId);
        Integer power = BaseContext.getPower();
        if(power != 1) {
            if (!uid.equals(userId)) {
                throw new UserPowerLowException(MessageConstant.NOPOWER_TOUPDATE);
            }
        }
        this.logMapper.updatebyid(log);
    }
}

