package com.sky.controller;

//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//


import com.sky.constant.JwtClaimsConstant;
import com.sky.context.BaseContext;
import com.sky.pojo.*;
import com.sky.properties.JwtProperties;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.UserService;
import com.sky.utils.JwtUtil;
import io.swagger.annotations.Api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping({"/user/user"})
@Api(
        tags = {"user用户相关接口"}
)
@CrossOrigin
@Slf4j
public class UserController {

    @Autowired
    private UserService userService;
    @Autowired
    private JwtProperties jwtProperties;


    public UserController() {
    }

    @PostMapping({"/logout"})
    public Result<String> logout() {
        userService.logout();
        return Result.success();
    }

    @PostMapping({"/login"})
    public synchronized Result<UserLoginVO>  login(@RequestBody UserLoginDTO userLoginDTO) {
        User user = this.userService.login(userLoginDTO);
        Map<String, Object> claims = new HashMap();
        claims.put(JwtClaimsConstant.USER_ID, user.getId());
        String token = JwtUtil.createJWT(this.jwtProperties.getAdminSecretKey(), this.jwtProperties.getAdminTtl(), claims);
        UserLoginVO build = UserLoginVO.builder().id(user.getId()).userPower(user.getUserPower()).username(user.getUsername()).token(token).build();
        return Result.success(build);
    }

    @PutMapping
    public Result updateuser(@RequestBody UserDTO userDTO) {
        this.userService.updateuser(userDTO);
        return Result.success();
    }

    @PutMapping("/power")
    public Result updateuserpower(@RequestBody UserDTO userDTO) {
        this.userService.updateuserpower(userDTO);
        return Result.success();
    }
//    @PutMapping("/power")
//    public Result updateuserpower(@RequestBody UserDTO userDTO) {
//        this.userService.updateuserpower(userDTO);
//        return Result.success();
//    }

    @GetMapping
    public Result<User> selectbyid() {
        Long id = BaseContext.getUserId();
        User user = this.userService.selectbyid(id);
        return Result.success(user);
    }
    @GetMapping({"/page"})
    public Result<PageResult> page(UserPageQueryDTO userPageQueryDTO) {
        PageResult page = userService.page(userPageQueryDTO);
        return Result.success(page);
    }
    @GetMapping({"/pagescore"})
    public Result<PageResult> pagescore(UserPageQueryDTO userPageQueryDTO) {
        PageResult page = userService.pagescore(userPageQueryDTO);
        return Result.success(page);
    }

    @PostMapping({"/register"})
    public Result register(@RequestBody Register register) {
        this.userService.register(register);
        return Result.success();
    }

    @PostMapping({"/email/{email}"})
    public Result sendmail(@PathVariable String email) {
        this.userService.sendmail(email);
        return Result.success();
    }

    @DeleteMapping
    public Result delectbyid(@RequestParam List<Long> ids) {
        userService.delectbyuserid(ids);
        return Result.success();
    }
    @PutMapping("/findpassword")
    public Result findpassword(@RequestBody UserFindPassword userFindPassword)
    {
        userService.findpassword(userFindPassword);
        return Result.success();
    }
}
