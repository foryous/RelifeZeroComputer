package com.sky.service.impl;

import com.fasterxml.jackson.databind.ser.Serializers;
import com.sky.constant.MessageConstant;
import com.sky.context.BaseContext;
import com.sky.controller.MessageController;
import com.sky.exception.UserPowerLowException;
import com.sky.mapper.SkyMapper;
import com.sky.pojo.Sky;
import com.sky.service.SkyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class SkyServiceImpl implements SkyService {
    @Autowired
    private SkyMapper skyMapper;
    @Override
    public void updatebyid(Sky sky) {
        Integer power = BaseContext.getPower();
        if(power != 1)
        {
            throw new UserPowerLowException(MessageConstant.NOPOWER_TOUPDATE);
        }
        skyMapper.update(sky);
    }

    @Override
    public void deletebyids(List<Long> ids) {
        Integer power = BaseContext.getPower();
        if(power != 1)
        {
            throw new UserPowerLowException(MessageConstant.NOPOWER_TOUPDATE);
        }
        for (Long id : ids) {
            skyMapper.delete(id);
        }
    }

    @Override
    public void addsky(Sky sky) {
        Integer power = BaseContext.getPower();
        Long userId = BaseContext.getUserId();
        if(power != 1)
        {
            throw new UserPowerLowException(MessageConstant.NOPOWER_TOUPDATE);
        }
        sky.setCreateUser(userId);
        skyMapper.addsky(sky);
    }
}
