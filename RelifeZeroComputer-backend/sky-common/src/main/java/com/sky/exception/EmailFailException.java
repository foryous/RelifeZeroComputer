package com.sky.exception;

public class EmailFailException extends BaseException{
    public EmailFailException() {
    }

    public EmailFailException(String msg) {
        super(msg);
    }
}
