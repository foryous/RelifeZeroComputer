package com.sky.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MessagePageQueryDTO implements Serializable {
    private int page;
    private int pageSize;
    private String title;
    private String username;
    private String content;
    private Long createUser;
}
