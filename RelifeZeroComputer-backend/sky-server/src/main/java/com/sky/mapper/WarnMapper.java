package com.sky.mapper;

import com.github.pagehelper.Page;
import com.sky.annotation.AutoFill;
import com.sky.enumeration.OperationType;
import com.sky.pojo.Warn;
import com.sky.pojo.WarnPageQueryDTO;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface WarnMapper {
    @AutoFill(OperationType.INSERT)
    void addWarn(Warn warn);

    Page<Warn> findpage(WarnPageQueryDTO warnPageQueryDTO);

    @Delete("delete from log.imformation where id=#{id}")
    void deletebyid(Long id);

    void updatebyid(Warn warn);

    @Select("select * from log.imformation where id=#{id}")
    Warn selectbyid(Long id);

    @Select("select * from log.imformation")
    List<Warn> get();
}
