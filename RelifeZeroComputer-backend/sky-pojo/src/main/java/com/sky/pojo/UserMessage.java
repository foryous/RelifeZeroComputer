package com.sky.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserMessage implements Serializable {
    private static final long serialVersionUID = 46412442243484364L;
    private String userEmail;
    private String code;
}
