package com.sky.task;


import com.sky.context.BaseContext;
import com.sky.mapper.UserLoginMapper;
import com.sky.mapper.UserMapper;
import com.sky.pojo.Remind;
import com.sky.pojo.User;
import com.sky.pojo.UserLogin;
import com.sky.utils.MailUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

@Component
@Slf4j
public class RmindTask {

    private final static Integer FAIL_STUDY = 0;
    private final static Integer LOG_OUT = 0;

    private final static Integer LOG_IN=1;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private UserLoginMapper userLoginMapper;

    @Scheduled(cron = "0 0 18 * * ? ")
    public void sendremindemail() {
        log.info("提醒打卡");
        Remind remind=new Remind();
        remind.setShowStatus(FAIL_STUDY);
        remind.setRemindStatus(1);
        List<User> users = userMapper.selectbystudystatusandremind(remind);
        for (User user : users) {
            String email = user.getEmail();
            MailUtils.sendEmail(email, "提醒打卡", "您今天还未打卡");
        }
    }

    @Scheduled(cron = "0 0 0 * * ?  ")
    public void updatestudaystatus() {
        List<User> users = userMapper.selectbystudystatus(FAIL_STUDY);
        for (User user : users) {
            user.setDuringtime(0);
            userMapper.updateUser(user);
        }
        userMapper.updateUserstudystatus(FAIL_STUDY);
    }

    @Scheduled(cron = "0 0 0/2 * * ?  ")
    public void userlogout() {
        List<User> users=userMapper.selectbyuserlogin(LOG_IN);
        for (User user : users) {
            Long id = user.getId();
            LocalDateTime lastime = user.getLastime();
            LocalDateTime now = LocalDateTime.now();
            UserLogin userLogin =new UserLogin();
            userLogin.setUserId(id);
            userLogin.setUserLogin(LOG_OUT);
            userLogin.setLogoutTime(now);
            int lastimeHour = lastime.getHour();
            int nowhour = now.getHour();
            if (lastimeHour >= 21 && nowhour <= 2) {
                nowhour = nowhour + 24;
                int cha = nowhour - lastimeHour;
                if (cha >= 2) {
                    user.setUserLogin(LOG_OUT);
                    user.setLogoutTime(now);
                    userMapper.updateUser(user);
                    userLoginMapper.updatebyid(userLogin);
                }
            } else {
                int cha = nowhour - lastimeHour;
                if (cha >= 2) {
                    user.setUserLogin(LOG_OUT);
                    user.setLogoutTime(now);
                    userMapper.updateUser(user);
                    userLoginMapper.updatebyid(userLogin);
                }
            }
        }
    }

}
