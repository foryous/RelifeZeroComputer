package com.sky.context;

import javax.imageio.stream.IIOByteBuffer;

public class BaseContext {

    public static ThreadLocal<Long> threadLocal = new ThreadLocal();
    public static ThreadLocal<Integer> threadLocal2 = new ThreadLocal();
    public static ThreadLocal<Integer> threadLocal3 = new ThreadLocal();

    public BaseContext() {
    }

    public static Integer getStatus() {
        return threadLocal3.get();
    }

    public static void setStatus(Integer id) {
        threadLocal3.set(id);
    }

    public static void setPower(Integer id) {
        threadLocal2.set(id);
    }

    public static Integer getPower() {
        return  threadLocal2.get();
    }

    public static void setUserId(Long id) {
        threadLocal.set(id);
    }

    public static Long getUserId() {
        return  threadLocal.get();
    }

    public static void removeCurrentId() {
        threadLocal.remove();
    }


}
