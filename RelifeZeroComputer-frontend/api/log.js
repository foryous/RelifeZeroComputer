import {get,post,put,dele} from "./request";

// 用户登陆
export function Log(params) {
  return post("/user/user/login",params)
}
// 用户注册
export function Reg(params) {
  return post('/user/user/register',params)
}
//退出登陆
export function Logout() {
   return post('/user/user/logout')
}
//忘记密码
export function Findpassword(params) {
  return put('/user/user/findpassword',params)
}
//用户信息返回
export function Selectpesrson() {
  return get('/user/user')
}
//用户信息修改 
export function Updatauser(params) {
  return put('/user/user',params)
}
//邮箱发送
export function Eamil(params) {
  return post('/user/user/email/'+params)
}

//加载日志
export function Page(params) {
    return get('/user/log/page', params)
}

//新增日志
export function Addcontenet(params){
  return post('/user/log',params)
}

//修改日志
  export function Updatabyid(params){
    return put("/user/log",params)
  }

//删除日志
export function Delectbyid(params) {
  return dele("/user/log?ids="+params)
}

//查询日志
export function Selcetbyid(params) {
  return get('/user/log/'+params)
}

//添加文章
export function Addmessage(params) {
  return post('/user/message',params)
}

//修改文章
export function Updatemsg(params) {
  return put('/user/message',params)
}

//删除文章
export function Delemsg(params) {
  return dele('/user/message?ids='+params)
}

//显示所有文章
export function Pagemsg() {
  return get('/user/message/page') 
}

//查询文章(id)
export function Selectmsg(params) {
  return get('/user/message/'+params)
}

//修改警告
export function Updatewarn(params) {
  return post('/user/warn',params)
}

//添加警告
export function Addwarn(params) {
  return put('/user/warn',params)
}

//删除警告
export function Delewarn(params) {
  return dele('/user/warn?ids='+params)
}

//获取所有警告信息
export function Pagewarn () {
  return get('/user/warn/page')
}

//查询某个警告{id}
export function Slecetwarn(params) {
  return get('/user/warn/'+params)
}

//日志管理积分榜
export function Pagescore(params) {
  return get('/user/user/pagescore',params)
}

//图片文件上传接口
export function Upload(params) {
  return post('/upload',params)
}

//管理端返回所有用户信息
export function Pageuser(params){
  return get('/user/user/page',params)
}

//修改用户权限
export function Updatauserpower(params){
  return put('/user/user/power',params)
}

//添加滚动条
export function Addsky(params){
  return post('/user/sky',params)
}

//修改滚动条内容
export function Updatasky(params) {
  return put('/user/sky',params)
}

//删除滚动条内容
export function Delesky(params) {
  return dele('/user/sky?ids='+params)
}

//查询滚动条内容
export function Pagesky() {
  return get('/user/sky/page')
}


  
  
  
  
  
  
  
  
  
  
  
  
  