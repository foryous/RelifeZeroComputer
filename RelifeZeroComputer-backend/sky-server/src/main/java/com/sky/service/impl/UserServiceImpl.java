package com.sky.service.impl;

//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//


import com.fasterxml.jackson.databind.ser.Serializers;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.JwtClaimsConstant;
import com.sky.constant.MessageConstant;
import com.sky.constant.StatusConstant;
import com.sky.context.BaseContext;
import com.sky.exception.*;
import com.sky.mapper.LogMapper;
import com.sky.mapper.LogUserMapper;
import com.sky.mapper.UserLoginMapper;
import com.sky.mapper.UserMapper;
import com.sky.pojo.*;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.UserService;
import com.sky.utils.MailUtils;

import java.time.LocalDateTime;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Service
public class UserServiceImpl implements UserService {
    private static final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private LogUserMapper logUserMapper;
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private LogMapper logMapper;
    @Autowired
    private UserLoginMapper userLoginMapper;

    private static final Integer USER_LOGIN = 1;

    public UserServiceImpl() {
    }

    public User login(UserLoginDTO userLoginDTO) {
        String username = userLoginDTO.getUsername();
        String password = userLoginDTO.getPassword();
        User user = this.userMapper.getByUsername(username);
        if (user == null) {
            throw new AccountNotFoundException(MessageConstant.ACCOUNT_NOT_FOUND);
        } else if (!password.equals(user.getPassword())) {
            throw new PasswordErrorException(MessageConstant.PASSWORD_ERROR);
        } else if (StatusConstant.DISABLE.equals(user.getStatus())) {
            throw new AccountLockedException(MessageConstant.ACCOUNT_LOCKED);
        } else {
            LocalDateTime logintime = LocalDateTime.now();
            LocalDateTime lastime = LocalDateTime.now();
            Long id = user.getId();
            User userlogin = new User();
            userlogin.setId(id);
            userlogin.setLastime(lastime);
            userlogin.setLoginTime(logintime);
            userlogin.setUserLogin(USER_LOGIN);
            UserLogin.UserLoginBuilder userLogin = UserLogin.builder()
                    .userId(id)
                    .loginTime(logintime)
                    .userLogin(USER_LOGIN);
            userLoginMapper.addimform(userLogin);
            userMapper.updateUser(userlogin);
            return user;
        }
    }

    public void updateuser(UserDTO userDTO) {

        User user = new User();
        Long id = userDTO.getId();
        BeanUtils.copyProperties(userDTO, user);
        String username = user.getUsername();
        User selectbyid = userMapper.selectbyusername(username);
        if (selectbyid != null && !selectbyid.getUsername().equals(username)) {

            throw new UsernameExistException(MessageConstant.USERNAME_EXIST);

        }

        List<Long> list = this.logUserMapper.selectbyuserid(user.getId());
        Iterator var4 = list.iterator();
        Integer showStatus = user.getShowStatus();
        Integer power = BaseContext.getPower();
        Long userId = BaseContext.getUserId();
        if (user.getUserPower() != null || user.getStatus() != null) {
            throw new UserPowerLowException(MessageConstant.NOPOWER_TOUPDATE);
        }
        if (power != 1) {
            if (!userId.equals(id)) {
                throw new UserPowerLowException(MessageConstant.NOPOWER_TOUPDATE);
            }
        }
        while (var4.hasNext()) {
            Long aLong = (Long) var4.next();
            NameId nameId = new NameId();
            nameId.setUsername(user.getUsername());
            nameId.setLogId(aLong);
            nameId.setShowStatus(showStatus);
            this.logMapper.updateusernamebyid(nameId);
        }
        this.userMapper.updateUser(user);
    }

    public User selectbyid(Long id) {
        return this.userMapper.selectbyid(id);
    }

    public void register(Register register) {
        ValueOperations valueOperations = this.redisTemplate.opsForValue();
        String code = (String) valueOperations.get("CODE:" + register.getEmail());
        if (!register.getCode().equals(code)) {
            throw new CodeMistakeException(MessageConstant.CODE_MISTAKE);
        } else {
            User user = new User();
            BeanUtils.copyProperties(register, user);
            user.setLastime(LocalDateTime.now());
            User user1 = this.userMapper.selectbyemail(user.getEmail());
            if (user1 != null) {
                throw new EmailExistException(MessageConstant.EMAIL_EXIST);
            } else {
                User user2 = this.userMapper.selectbyusername(user.getUsername());
                if (user2 != null) {
                    throw new UsernameExistException(MessageConstant.USERNAME_EXIST);
                } else {
                    this.userMapper.insertuser(user);
                    Long id = user.getId();
                    valueOperations.set(JwtClaimsConstant.USER_STATUS + id, "1");
                    valueOperations.set(JwtClaimsConstant.USER_POWER + id, "0");
                }
            }
        }
    }

    public void sendmail(String email) {
        String code = this.generateCode();
        ValueOperations valueOperations = this.redisTemplate.opsForValue();
        valueOperations.set("CODE:" + email, code, 5L, TimeUnit.MINUTES);
        MailUtils.sendEmail(email, "验证码", "您的验证码为:" + code + "，有效期为5分钟");
    }

    @Override
    public PageResult page(UserPageQueryDTO userPageQueryDTO) {
        PageHelper.startPage(userPageQueryDTO.getPage(), userPageQueryDTO.getPageSize());
        Page<User> page = userMapper.findpage(userPageQueryDTO);
        long total = page.getTotal();
        List<User> result = page.getResult();
        return new PageResult(total, result);
    }

    @Override
    public void delectbyuserid(List<Long> ids) {
        ValueOperations valueOperations = redisTemplate.opsForValue();
        Integer power = BaseContext.getPower();
        Long userId = BaseContext.getUserId();
        for (Long id : ids) {
            if (power != 1) {
                if (!userId.equals(id)) {
                    throw new UserPowerLowException(MessageConstant.NOPOWER_TODELETE);
                }
            }
            valueOperations.decrement(JwtClaimsConstant.USER_POWER + id);
            valueOperations.decrement(JwtClaimsConstant.USER_STATUS + id);
            userMapper.deletebyid(id);
            List<Long> logids = logUserMapper.selectbyuserid(id);
            logUserMapper.deletebyuserid(id);
            for (Long logid : logids) {
                logMapper.delectbyid(logid);
            }
        }
    }

    @Override
    public void updateuserpower(UserDTO userDTO) {
        if (!BaseContext.getPower().equals(1)) {
            throw new UserPowerLowException(MessageConstant.NOPOWER_TOUPDATE);
        }
        ValueOperations valueOperations = redisTemplate.opsForValue();
        Long id = userDTO.getId();
        valueOperations.getOperations().delete(JwtClaimsConstant.USER_STATUS + id);
        valueOperations.getOperations().delete(JwtClaimsConstant.USER_POWER + id);
        User user = new User();
        BeanUtils.copyProperties(userDTO, user);
        userMapper.updateUser(user);
        Integer status = userDTO.getStatus();
        Integer power = userDTO.getUserPower();
        String userStatus = String.valueOf(status);
        String userPower = String.valueOf(power);
        valueOperations.set(JwtClaimsConstant.USER_STATUS + id, userStatus);
        valueOperations.set(JwtClaimsConstant.USER_POWER + id, userPower);
    }

    @Override
    public void logout() {
        Long userId = BaseContext.getUserId();
        LocalDateTime now = LocalDateTime.now();
        User user = new User();
        UserLogin userLogin = new UserLogin();
        userLogin.setUserLogin(0);
        userLogin.setUserId(userId);
        userLogin.setLogoutTime(now);
        user.setId(userId);
        user.setLogoutTime(now);
        user.setUserLogin(0);
        userLoginMapper.updatebyid(userLogin);
        userMapper.updateUser(user);
    }

    @Override
    public PageResult pagescore(UserPageQueryDTO userPageQueryDTO) {
        PageHelper.startPage(userPageQueryDTO.getPage(), userPageQueryDTO.getPageSize());
        Page<User> page = userMapper.findpageorderbyscore(userPageQueryDTO);
        long total = page.getTotal();
        List<User> result = page.getResult();
        return new PageResult(total, result);
    }

    @Override
    public void findpassword(UserFindPassword userFindPassword) {
        ValueOperations valueOperations = this.redisTemplate.opsForValue();
        String code = (String) valueOperations.get("CODE:" + userFindPassword.getEmail());
        if (!userFindPassword.getCode().equals(code)) {
            throw new CodeMistakeException(MessageConstant.CODE_MISTAKE);
        } else {
            User user = new User();
            BeanUtils.copyProperties(userFindPassword, user);
            User selectbyusername = userMapper.selectbyusername(user.getUsername());
            String email = selectbyusername.getEmail();
            String userEmail = user.getEmail();
            if (!email.equals(userEmail)) {
                throw new EmailFailException(MessageConstant.EMAILNOTRIGHT);
            }
            userMapper.updateUserbyusername(user);
        }
    }


    public String generateCode() {
        Random randObj = new Random();
        return Integer.toString(100000 + randObj.nextInt(900000));
    }
}
