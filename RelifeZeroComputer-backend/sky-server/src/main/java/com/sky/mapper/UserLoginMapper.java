package com.sky.mapper;

import com.sky.pojo.UserLogin;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface UserLoginMapper {

    void addimform(UserLogin.UserLoginBuilder userLogin);

    void updatebyid(UserLogin userLogin);

    @Select("select * from log.user_login where user_id=#{userId}")
    UserLogin selectbyuserid(Long userId);
}
