package com.sky.mapper;

//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//



import com.github.pagehelper.Page;
import com.sky.annotation.AutoFill;
import com.sky.enumeration.OperationType;
import com.sky.pojo.Log;
import com.sky.pojo.LogPageQueryDTO;
import com.sky.pojo.NameId;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface LogMapper {
    @AutoFill(OperationType.INSERT)
    void addcontent(Log log);

    @Delete({"delete from log.log where id=#{id}"})
    void delectbyid(Long id);

    Page<Log> findpage(LogPageQueryDTO logPageQueryDTO);

    @Select({"select * from log.log where id=#{id}"})
    Log selectbyid(Long id);

    @AutoFill(OperationType.UPDATE)
    void updatebyid(Log log);

    void updateusernamebyid(NameId nameId);
}
