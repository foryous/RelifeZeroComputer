package com.sky.service;

import com.sky.pojo.MessageDTO;
import com.sky.pojo.MessagePageQueryDTO;
import com.sky.pojo.MessageVO;
import com.sky.result.PageResult;

import java.util.List;


public interface MessageService {
     void deletebyids(List<Long> ids) ;
    void addmessage(MessageDTO messageDTO);
    MessageVO selectbyid(Long id);
    void updatebyid(MessageDTO messageDTO);

    PageResult page(MessagePageQueryDTO messagePageQueryDTO);
}
