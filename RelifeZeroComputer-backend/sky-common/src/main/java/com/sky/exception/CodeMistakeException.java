package com.sky.exception;

public class CodeMistakeException extends BaseException{
    public CodeMistakeException() {
    }

    public CodeMistakeException(String message) {
        super(message);
    }
}
