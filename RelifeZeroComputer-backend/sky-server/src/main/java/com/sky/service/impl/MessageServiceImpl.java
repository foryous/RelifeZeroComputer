package com.sky.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.MessageConstant;
import com.sky.context.BaseContext;
import com.sky.exception.UserPowerLowException;
import com.sky.mapper.MessageMapper;
import com.sky.pojo.*;
import com.sky.result.PageResult;
import com.sky.service.MessageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class MessageServiceImpl implements MessageService {


    @Autowired
    private MessageMapper messageMapper;

    @Override
    public void deletebyids(List<Long> ids) {
        Integer power = BaseContext.getPower();
        if(power == 0)
        {
            throw new UserPowerLowException(MessageConstant.NOPOWER_TOADD);
        }
        for (Long id : ids) {
            messageMapper.deletebyid(id);
        }

    }

    @Override
    public void addmessage(MessageDTO messageDTO) {
        Integer power = BaseContext.getPower();
        Long userId = BaseContext.getUserId();
        if(power == 0)
        {
            throw new UserPowerLowException(MessageConstant.NOPOWER_TOADD);
        }
        Message message=new Message();
        messageDTO.setCreateUser(userId);
        BeanUtils.copyProperties(messageDTO, message);
        messageMapper.addmessage(message);
    }

    @Override
    public MessageVO selectbyid(Long id) {

        MessageVO messageVO=messageMapper.selectbyid(id);
        return messageVO;
    }

    @Override
    public void updatebyid(MessageDTO messageDTO) {
        Integer power = BaseContext.getPower();
        Long userId = BaseContext.getUserId();
        if(power == 0)
        {
            throw new UserPowerLowException(MessageConstant.NOPOWER_TOUPDATE);
        }
        messageDTO.setCreateUser(userId);
        Message message=new Message();
        BeanUtils.copyProperties(messageDTO,message);
        messageMapper.updatebyid(message);
    }

    @Override
    public PageResult page(MessagePageQueryDTO messagePageQueryDTO) {
        PageHelper.startPage(messagePageQueryDTO.getPage(), messagePageQueryDTO.getPageSize());
        Page<Message> page = messageMapper.findpage(messagePageQueryDTO);
        long total = page.getTotal();
        List<Message> result = page.getResult();
        return new PageResult(total, result);
    }

}
