package com.sky.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Register implements Serializable {
    private Long id;
    private Integer userPower;
    private String username;
    private String password;
    private String email;
    private Long sex;
    private String avatar;
    private Integer status;
    private String code;
}
