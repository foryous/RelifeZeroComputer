package com.sky.mapper;

import com.sky.annotation.AutoFill;
import com.sky.enumeration.OperationType;
import com.sky.pojo.Sky;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface SkyMapper {
    @Select("select * from log.sky")
    List<Sky> get();

    @Delete("delete from log.sky where id=#{id}")
    void delete(Long id);

    @AutoFill(OperationType.UPDATE)
    void update(Sky sky);

    @AutoFill(OperationType.INSERT)
    void addsky(Sky sky);
}
