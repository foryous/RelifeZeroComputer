package com.sky.exception;

public class UserFailLoginException extends BaseException{
    public UserFailLoginException(String msg) {
        super(msg);
    }

    public UserFailLoginException() {
    }
}
