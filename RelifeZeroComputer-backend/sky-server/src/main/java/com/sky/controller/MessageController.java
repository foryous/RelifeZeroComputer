package com.sky.controller;

import com.sky.mapper.MessageMapper;
import com.sky.pojo.*;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.MessageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("user/message")
@Slf4j
@CrossOrigin
public class MessageController {
    @Autowired
    private MessageService messageService;
    @Autowired
    private MessageMapper messageMapper;

    @PostMapping
    public Result addmessage(@RequestBody MessageDTO MessageDTO) {
        messageService.addmessage(MessageDTO);
        return Result.success();
    }
    @GetMapping("/{id}")
    public Result<MessageVO> selectbyid(@PathVariable Long id)
    {
        MessageVO messageVO=messageService.selectbyid(id);
        return Result.success(messageVO);
    }
    @DeleteMapping
    public Result deletebyids(@RequestParam List<Long> ids)
    {
        messageService.deletebyids(ids);
        return Result.success();
    }
    @PutMapping
    public Result updatebyid(@RequestBody MessageDTO messageDTO)
    {
        messageService.updatebyid(messageDTO);
        return Result.success();
    }
    @GetMapping({"/page"})
    public Result<List<Message>> page() {
     List<Message> list=messageMapper.get();
     return Result.success(list);
    }

}
