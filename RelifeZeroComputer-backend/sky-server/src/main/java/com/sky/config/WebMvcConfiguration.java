

package com.sky.config;

import com.sky.interceptor.JwtTokenUserInterceptor;
import com.sky.json.JacksonObjectMapper;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class WebMvcConfiguration extends WebMvcConfigurationSupport {
    private static final Logger log = LoggerFactory.getLogger(WebMvcConfiguration.class);
    @Autowired
    private JwtTokenUserInterceptor jwtTokenUserInterceptor;

    public WebMvcConfiguration() {
    }

    protected void addInterceptors(InterceptorRegistry registry) {
        log.info("开始注册自定义拦截器...");
        registry.addInterceptor(this.jwtTokenUserInterceptor)
                .addPathPatterns("/user/**")
                .excludePathPatterns("/user/user/login")
                .excludePathPatterns("/user/user/email/{email}")
                .excludePathPatterns("/user/user/register")
                .excludePathPatterns("/upload")
                .excludePathPatterns("/user/user/findpassword");
    }

    @Bean
    public Docket docket() {
        ApiInfo apiInfo = (new ApiInfoBuilder())
                .title("从0开始的计算机生活接口文档")
                .version("2.0")
                .description("从0开始的计算机生活接口文档").build();
        Docket docket = (new Docket(DocumentationType.SWAGGER_2))
                .apiInfo(apiInfo).select()
                .apis(RequestHandlerSelectors
                        .basePackage("com.sky.controller"))
                .paths(PathSelectors.any()).build();
        return docket;
    }

    protected void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler(new String[]{"/doc.html"}).addResourceLocations(new String[]{"classpath:/META-INF/resources/"});
        registry.addResourceHandler(new String[]{"/webjars/**"}).addResourceLocations(new String[]{"classpath:/META-INF/resources/webjars/"});
    }

    protected void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
        log.info("开启自定义消息转换器");
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converter.setObjectMapper(new JacksonObjectMapper());
        converters.add(0, converter);
    }
}
