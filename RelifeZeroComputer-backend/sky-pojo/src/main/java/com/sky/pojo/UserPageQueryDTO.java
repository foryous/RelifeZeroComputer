package com.sky.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserPageQueryDTO implements Serializable {
    private int page;
    private int pageSize;
    private String username;
    private LocalDateTime createTime;
    private LocalDateTime updateTime;
}
