package com.sky.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserLogin implements Serializable {

    private Long id;
    private Long userId;
    private Integer userLogin;
    private LocalDateTime loginTime;
    private LocalDateTime logoutTime;
}
