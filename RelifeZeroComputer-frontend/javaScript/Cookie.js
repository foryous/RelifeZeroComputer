
//从cookie读取用户某个数据
 function getCookie(name) {
    var cookieName = name + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var cookieArray = decodedCookie.split(';');

    for (var i = 0; i < cookieArray.length; i++) {
        var cookie = cookieArray[i];
        while (cookie.charAt(0) == ' ') {
            cookie = cookie.substring(1);
        }
        if (cookie.indexOf(cookieName) == 0) {
            return cookie.substring(cookieName.length, cookie.length);
        }
    }
    return null;
}

//设置用户储存的信息cookie
 function setCookie(name, value, timeout) {
    let expires = "";
    if (timeout) {
        let Time = new Date();
        Time.setTime(Time.getTime() + (timeout * 60 * 1000));
        expires = "; expires=" + Time.toUTCString();
    }
    document.cookie = name + "=" + encodeURIComponent(value) + expires + "; path=/";
}

//从cookie中读取用户的所有数据
export function Getfromcookie() {
    var user ={
        token:"",
    };
    user.token=getCookie("usertoken");
    return user;
}

//从cookie中设置用户所有数据
export function Setincookie(Value) {
    //存储120分钟
    if(Value.token){setCookie("usertoken", Value.token, 120);}
}

//删除用户的数据
export function delformcookie() {
    //setCookie("usertoken",null,0);
    document.cookie = `${encodeURIComponent("usertoken")}=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;`;
}