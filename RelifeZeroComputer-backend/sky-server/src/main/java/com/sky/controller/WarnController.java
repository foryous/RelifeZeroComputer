package com.sky.controller;

import com.sky.mapper.WarnMapper;
import com.sky.pojo.Message;
import com.sky.pojo.Warn;
import com.sky.pojo.WarnPageQueryDTO;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.WarnService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping({"/user/warn"})
@CrossOrigin
public class WarnController {

    @Autowired
    private WarnService warnService;
    @Autowired
    private WarnMapper warnMapper;

    @PutMapping
    public Result addWarn(@RequestBody Warn warn) {
        warnService.addWarn(warn);
        return Result.success();
    }

    @GetMapping("page")
    public Result<List<Warn>> pagewarn() {
        List<Warn> list=warnMapper.get();
        return Result.success(list);
    }

    @DeleteMapping
    public Result deletebyid(@RequestParam List<Long> ids) {
        warnService.deletebyids(ids);
        return Result.success();
    }

    @PostMapping
    public Result updatebyid(@RequestBody Warn warn) {
        warnService.updatebyid(warn);
        return Result.success();
    }

    @GetMapping("/{id}")
    public Result<Warn> selectbyid(@PathVariable Long id) {
        Warn warn=warnService.selectbyid(id);
        return Result.success(warn);

    }

}

