package com.sky.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public  class UserVO {
    private Long id;
    private Integer userPower;
    private String username;
    private String password;
    private String email;
    private Long sex;
    private String avatar;
    private Integer status;
    private Integer showStatus;
}
