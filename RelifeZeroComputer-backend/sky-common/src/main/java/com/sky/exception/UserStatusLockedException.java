package com.sky.exception;

public class UserStatusLockedException extends BaseException{
    public UserStatusLockedException() {
    }

    public UserStatusLockedException(String msg) {
        super(msg);
    }
}
