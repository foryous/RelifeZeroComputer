package com.sky.controller;

//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//


import com.sky.mapper.UserMapper;
import com.sky.result.Result;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@CrossOrigin
@Slf4j
public class UploadController {
    @Autowired
    private UserMapper userMapper;

    @PostMapping({"/upload"})
    public Result<String> upload(MultipartFile file) {
        String originalFilename = file.getOriginalFilename();
        String extension = originalFilename.substring(originalFilename.lastIndexOf("."));
        String objectName = UUID.randomUUID().toString() + extension;

        try {
            file.transferTo(new File("/www/wwwroot/ceshi/static/image/" + objectName));
            String filename = "../../static/image/" + objectName;
            return Result.success(filename);
        } catch (IOException var6) {
            log.error("文件上传失败:{}", var6.toString());
            return Result.error("文件上传失败");
        }
    }
}

