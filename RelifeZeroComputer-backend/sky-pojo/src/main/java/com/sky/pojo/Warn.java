package com.sky.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Warn implements Serializable {

    private Long id;
    private String warn;
    private String title;
    private LocalDateTime createTime;
    private LocalDateTime updateTime;
}
