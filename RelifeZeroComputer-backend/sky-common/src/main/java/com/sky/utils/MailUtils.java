package com.sky.utils;

import com.sky.constant.MessageConstant;
import com.sky.exception.EmailFailException;
import java.util.Date;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.Message.RecipientType;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
@Slf4j
public class MailUtils {
    private static final String emailProtocol = "smtp";
    private static final String emailSMTPHost = "smtp.qq.com";
    private static final String emailPort = "465";
    private static final String emailAccount = "1991282164@qq.com";
    private static final String emailPassword = "krvjwemnpatrbegh";

    public static boolean sendEmail(String emails, String title, String content) {
        if (emails != null && !emails.isEmpty()) {
            try {
                Properties props = new Properties();
                props.setProperty("mail.transport.protocol", emailProtocol);
                props.setProperty("mail.smtp.host", emailSMTPHost);
                props.setProperty("mail.smtp.port", emailPort);
                props.setProperty("mail.smtp.auth", "true");
                props.put("mail.smtp.ssl.enable", "true");
                props.put("mail.smtp.ssl.protocols", "TLSv1.2");
                props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
                props.setProperty("mail.smtp.connectiontimeout", "10000");
                props.setProperty("mail.smtp.timeout", "10000");
                props.setProperty("mail.smtp.writetimeout", "10000");
                Session session = Session.getDefaultInstance(props);
                session.setDebug(false);
                MimeMessage message = new MimeMessage(session);
                message.setFrom(new InternetAddress(emailAccount, "从0开始的计算机生活官方", "UTF-8"));
                message.setRecipient(RecipientType.TO, new InternetAddress(emails, emails, "UTF-8"));
                message.setSubject(title, "UTF-8");
                message.setContent(content, "text/html;charset=UTF-8");
                message.setSentDate(new Date());
                message.saveChanges();
                Transport transport = session.getTransport();
                transport.connect(emailAccount, emailPassword);
                transport.sendMessage(message, message.getAllRecipients());
                transport.close();
                return true;
            } catch (Exception var7) {
                log.error("发送邮件失败,系统错误！", var7);
                throw new EmailFailException(MessageConstant.EMAIL_FAIL);
            }
        } else {
            return false;
        }
    }

//    public static void main(String[] args) {
//        Set<String> set = new HashSet();
//        set.add("1991282164@qq.com");
//        System.out.println(sendEmail("1991282164@qq.com", "测试发送邮件的接口！", "您好！这是我发送的一封测试发送接口的邮件，看完请删除记录。"));
//    }
}


