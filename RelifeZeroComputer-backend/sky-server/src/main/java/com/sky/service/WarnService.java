package com.sky.service;

import com.sky.pojo.Warn;
import com.sky.pojo.WarnPageQueryDTO;
import com.sky.result.PageResult;

import java.util.List;

public interface WarnService {
    void addWarn(Warn warn);

    PageResult page(WarnPageQueryDTO warnPageQueryDTO);

    void deletebyids(List<Long> ids);

    void updatebyid(Warn warn);

    Warn selectbyid(Long id);
}
