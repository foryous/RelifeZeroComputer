package com.sky.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class User implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    //0为普通用户
    private Integer userPower;
    private String username;
    private String password;
    private String email;
    private Long sex;
    private String avatar;
    //0为禁用
    private Integer status;
    //0为未打卡，1为已打卡
    private Integer studyStatus;
    //0为未登录，1为已登录
    private LocalDateTime lastime;
    //0为未登录，1为已登录
    private Integer userLogin;

    private LocalDateTime loginTime;

    private LocalDateTime logoutTime;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;

    private Long score;

    private Integer duringtime;
    //0为不展示qq，1为展示qq
    private Integer showStatus;
    //0为不提醒打卡，1为提醒打卡
    private Integer remindStatus;
}
