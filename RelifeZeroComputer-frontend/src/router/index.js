import Vue from 'vue'
import Router from 'vue-router'
import home from '@/components/home'
import firstweb from '@/components/themain/firstweb.vue'
import log from '@/components/log'
import { Getfromcookie,Setincookie } from '../../javaScript/Cookie'

Vue.use(Router)

/* Router.beforeEach((to,form,next)=>{
  console.log("正在检验token")
  next('/');
}) */
// 定义全局路由守卫
const authGuard = (to, from, next) => {
 



  if(Getfromcookie().token != null) {



    next()
  }
  else {
    next('/')
  }
  // 进行身份验证逻辑，根据需求自行实现
  // 如果验证失败，可以通过next函数跳转到指定页面
  // 例如：next('/login') 跳转到登录页
  // 验证成功则调用 next() 函数继续路由跳转
  /* const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
  const isAdmin = to.matched.some(record => record.meta.isAdmin);
  const currentUser = firebase.auth().currentUser;

  if (requiresAuth && !currentUser) {
    // 如果需要登录才能访问并且当前用户未登录，则跳转到登录页
    const checktoken=Getfromcookie().token;
    if(checktoken){
      next("/");
    }

  } else if (isAdmin && (!currentUser || !currentUser.isAdmin)) {
    // 如果需要超级管理员权限并且当前用户不是超级管理员，则跳转到错误页
    
    next({ path: "/error", query: { message: "没有权限访问该页面" } });
  } else {
    // 其他情况下，直接进入目标路由
    next();
  } */
}

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path:'/error',component:()=>import("../components/error.vue"),
      meta: {
        requiresAuth: false, // 不需要登录认证即可访问
      },
    },
    // 根路由定向到log.vue界面
    {
        path:'/',component:log,
        meta: {
          requiresAuth: false, // 不需要登录认证即可访问
        },
    },
    //home主界面 首次进入重定向到firstweb
    {
      path:'/home',component:home,
      meta: {
        requiresAuth: true, // 需要登录认证才能访问
        //isAdmin: true, // 需要超级管理员权限才能访问
      },
      redirect:'/firstweb',
      children:[
        {
          path:'/firstweb',
          component:firstweb,
          requiresAuth: true
        },
        {
          path:'/dataweb',
          requiresAuth: true,
          component:()=>import("../components/themain/dataweb.vue")
        },
        {
          path:'/person',component:()=>import("../components/themain/person.vue"),
          requiresAuth: true
        },
        {
          path:'/wenzhang',
          component:()=>import("../components/themain/wenzhang.vue"),
          requiresAuth: true
        },
        {
          path:'/controll',
          component:()=>import("../components/themain/controll.vue"),
          requiresAuth: true
        }
        
      ]
    }
  ],
  // 添加全局路由守卫
   beforeEach: authGuard
})


/* path: '/home',component:home,
      redirect:'/home/firstweb', */

      /* children:[
        {
          path:'/home/firstweb',
          component:firstweb,
        },
        {
          path:'dataweb',
          component:()=>import("../components/themain/dataweb.vue")
        },
        
      ] */
