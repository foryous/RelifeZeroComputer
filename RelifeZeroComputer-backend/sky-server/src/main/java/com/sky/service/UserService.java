package com.sky.service;

//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//



import com.sky.pojo.*;
import com.sky.result.PageResult;

import java.util.List;

public interface UserService {
    User login(UserLoginDTO userLoginDTO);

    void updateuser(UserDTO userDTO);

    User selectbyid(Long id);

    void register(Register register);

    void sendmail(String email);

    PageResult page(UserPageQueryDTO userPageQueryDTO);

    void delectbyuserid(List<Long> ids);

    void updateuserpower(UserDTO userDTO);

    void logout();

    PageResult pagescore(UserPageQueryDTO userPageQueryDTO);

    void findpassword(UserFindPassword userFindPassword);

    // updateuserpower(UserDTO userDTO);
}
