package com.sky.exception;

public class UserPowerLowException extends BaseException{
    public UserPowerLowException() {
    }

    public UserPowerLowException(String msg) {
        super(msg);
    }
}
