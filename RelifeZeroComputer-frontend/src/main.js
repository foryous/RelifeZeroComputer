// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
//引用element ui在全局
import ElementUI, { Message } from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import { Getfromcookie } from '../javaScript/Cookie'



Vue.use(ElementUI)

Vue.config.productionTip = false


/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})

router.beforeEach((to,form,next)=>{
  if(Getfromcookie().token) {
    next()
  }
  else {

    if(to.path === '/'){
      next()
    }
    else{
      next({
        path:'/'
      })
    }

    this.$message.error('登陆失效，重新登陆');
  }

})
