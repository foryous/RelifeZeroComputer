package com.sky.controller;

import com.sky.mapper.SkyMapper;
import com.sky.pojo.Message;
import com.sky.pojo.MessageDTO;
import com.sky.pojo.MessageVO;
import com.sky.pojo.Sky;
import com.sky.result.Result;
import com.sky.service.SkyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("user/sky")
@Slf4j
@CrossOrigin
public class SkyController {
    @Autowired
    private SkyService skyService;
    @Autowired
    private SkyMapper skyMapper;


    @PostMapping
    public Result addsky(@RequestBody Sky sky) {
        skyService.addsky(sky);
        return Result.success();
    }

    @DeleteMapping
    public Result deletebyids(@RequestParam List<Long> ids)
    {
        skyService.deletebyids(ids);
        return Result.success();
    }
    @PutMapping
    public Result updatebyid(@RequestBody Sky sky)
    {
        skyService.updatebyid(sky);
        return Result.success();
    }
    @GetMapping({"/page"})
    public Result<List<Sky>> page() {
        List<Sky> list=skyMapper.get();
        return Result.success(list);
    }
}
