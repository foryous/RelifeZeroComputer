package com.sky.constant;

public class JwtClaimsConstant {

    public static final String USER_STATUS = "USER_STATUS";
    public static final String USER_ID = "userId";
    public static final String USER_POWER = "USER_POWER";
    public static final String USERNAME = "username";
    public static final String NAME = "name";

}
