package com.sky.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Log implements Serializable {
    private Long id;
    private String username;
    private String createPd;
    private String contentLog;
    private String stuTime;
    private String logTime;
    private Integer deleteBoll;
    private LocalDateTime createTime;
    private LocalDateTime updateTime;
    private String bz;
    private String bz1;
    private String bz2;
    private String bz3;
    private String bz4;
    private String QQnumber;
    private Integer showStatus;
}