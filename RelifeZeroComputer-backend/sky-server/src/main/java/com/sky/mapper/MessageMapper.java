package com.sky.mapper;

import com.github.pagehelper.Page;
import com.sky.annotation.AutoFill;
import com.sky.enumeration.OperationType;
import com.sky.pojo.*;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface MessageMapper {
    @AutoFill(OperationType.INSERT)
    public void addmessage(Message message);

    @Select("select * from log.message where id=#{id}")
    MessageVO selectbyid(Long id);

    @Delete("delete from log.message where id=#{id}")
    void deletebyid(Long id);

    @AutoFill(OperationType.UPDATE)
    void updatebyid(Message message);

    Page<Message> findpage(MessagePageQueryDTO messagePageQueryDTO);

    @Select("select * from log.message order by create_time desc")
    List<Message> get();
}
