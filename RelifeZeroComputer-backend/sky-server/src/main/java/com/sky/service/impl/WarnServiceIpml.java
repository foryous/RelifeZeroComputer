package com.sky.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.JwtClaimsConstant;
import com.sky.constant.MessageConstant;
import com.sky.context.BaseContext;
import com.sky.exception.UserPowerLowException;
import com.sky.mapper.WarnMapper;
import com.sky.pojo.Warn;
import com.sky.pojo.WarnPageQueryDTO;
import com.sky.result.PageResult;
import com.sky.service.WarnService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class WarnServiceIpml implements WarnService {
    @Autowired
    private WarnMapper warnMapper;
    @Autowired
    private RedisTemplate redisTemplate;
    @Override
    public void addWarn(Warn warn) {
        Long userId = BaseContext.getUserId();
        ValueOperations valueOperations = redisTemplate.opsForValue();
        String power = (String) valueOperations.get(JwtClaimsConstant.USER_POWER + userId);
        Integer Power= Integer.valueOf(power);
        if(Power == 1)
        {
            warnMapper.addWarn(warn);
        } else {
            throw new UserPowerLowException(MessageConstant.NOPOWER_TOUPDATE);
        }
    }

    @Override
    public PageResult page(WarnPageQueryDTO warnPageQueryDTO) {
        PageHelper.startPage(warnPageQueryDTO.getPage(),warnPageQueryDTO.getPageSize());
        Page<Warn> page =warnMapper.findpage(warnPageQueryDTO);
        long total = page.getTotal();
        List<Warn> result = page.getResult();
        return new PageResult(total,result);
    }

    @Override
    public void deletebyids(List<Long> ids) {
        Long userId = BaseContext.getUserId();
        ValueOperations valueOperations = redisTemplate.opsForValue();
        String power = (String) valueOperations.get(JwtClaimsConstant.USER_POWER + userId);
        Integer Power= Integer.valueOf(power);
        if(Power == 1)
        {
            for (Long id : ids) {
                warnMapper.deletebyid(id);
            }

        } else {
            throw new UserPowerLowException(MessageConstant.NOPOWER_TOUPDATE);
        }
    }

    @Override
    public void updatebyid(Warn warn) {
        Long userId = BaseContext.getUserId();
        ValueOperations valueOperations = redisTemplate.opsForValue();
        String power = (String) valueOperations.get(JwtClaimsConstant.USER_POWER + userId);
        Integer Power= Integer.valueOf(power);
        if(Power == 1)
        {
            warnMapper.updatebyid(warn);
        } else {
            throw new UserPowerLowException(MessageConstant.NOPOWER_TOUPDATE);
        }
    }

    @Override
    public Warn selectbyid(Long id) {
        return warnMapper.selectbyid(id);
    }
}
